Transfer data from one GitLAB version (low) to another Gitlab (new) using GitLAB API and plain Python

servers.conf.example - example of servers.conf file  
status/check_gitlab_instance - script to check from GitLAB instance  
copy/copy_gitlab_users - script to copy users  
copy/copy_gitlab_group - script to copy groups (not neccery namespace)   
copy/copy_gitlab_projects - script to copy projects  
copy/copy_gitlab_issues - script to coppy issues  
copy/copy_gitlab_snippets - script to copy snippets  
